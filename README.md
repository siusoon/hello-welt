# hello world!

The project presents the speech-acts of human and machines, looping multiple Hello World programs written in different programming languages alongside a selection of human languages, combining them into a real-time, multilingual, machine-driven confusion of tongues (as in _The Tower of Babel_).

The original work was made in Adobe Flash by Geoff Cox and Duncan Shingleton in 2008. In this updated version, made in 2022, Geoff Cox and Winnie Soon are using JavaScript.

To run the program: https://siusoon.gitlab.io/hello-welt/hellowelt_program/

To see the data file: https://gitlab.com/siusoon/hello-welt/-/blob/main/hellowelt_program/data.json

## To update/add new hello world program

1. Fork the directory 
2. Update the JSON file by adding the new record that specifies the programming language and syntax. For the syntax, please use `\n` for the new line and `\` in front of the double quote if you want to show it.
3. You can copy and paste and validate your JSON file [here](https://jsonlint.com/)
4. Submit a pull request

## References / Inspirations 

- _The Hello World Collection_, maintained by Wolfram Rösler, http://helloworldcollection.de/
- CCSW working group on "hello, [other language(s) than English] world!" (2022 [Code Critique](https://wg.criticalcodestudies.com/index.php?p=/discussion/122/hello-other-language-s-than-english-world-2022-code-critique)) 
- _Speaking Code: Coding as Aesthetic and Political Expression_, by Geoff Cox and Alex McLean (MIT Press, 2012), https://speaking-code.lurk.org/
- _Aesthetic Programming: A Handbook of Software Studies_, by Winnie Soon and Geoff Cox (Open Humanities Press, 2020), https://aesthetic-programming.net/
