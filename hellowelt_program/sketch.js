let loadedJSON;
let datafiles;
let langfiles;
let string = "*Hello World*";

function preload() {
   loadedJSON = loadJSON('data.json', onFileload);
}

function onFileload() {
  console.log("File loaded successfully...");
}
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  setInterval(helloworld, 5500); //3500 final
}

function helloworld() {
  background(0);
  fill(220, 226, 229);
  textLeading(width * 0.03);
  textSize(width * 0.015);
  datafiles = loadedJSON.code;
  langfiles = loadedJSON.lang;
  let pick = int(random(datafiles.length));
  let finalpick = datafiles[pick].syntax
  let final_lang = int(random(langfiles.length));
  finalpick = finalpick.replaceAll(string, langfiles[final_lang].text);
  text(finalpick, 50, 50);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
